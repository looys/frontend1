"use strict";

// Создаём переменные под хранения элементов из html кода, берём их по классам из css
let form = document.querySelector("form");

let inputName = document.querySelector(".input1");
let mistakeName = document.querySelector(".input-mistake-name");

let inputGrade = document.querySelector(".input2");
let mistakeGrade = document.querySelector(".input-mistake-grade");


// Прописываем текст в input-ы полученный из локальной сессии браузера по ключам name, grade
inputName.value = localStorage.getItem("name");
inputGrade.value = localStorage.getItem("grade");


form.addEventListener("submit", validate);

// Ставим фокус для input-ов при вызове методов очистки
inputName.addEventListener("focus", clearmistakeName);
inputGrade.addEventListener("focus", clearmistakeGrade);


// Прописываем методы, что если текст в input вводится, вызвать методы changeName, changeGrade
inputName.addEventListener("input", changeName);
inputGrade.addEventListener("input", changeGrade);


// Описываем функцию валидации
function validate(event) {

    event.preventDefault();

    // Создаём переменные под хранение длмны input и символов
    let nameLength = inputName.value.trim().length;
    let NameSymbols = inputName.value.trim();
    let grade = +inputGrade.value.trim();

    // Если количество введённых символов в поле Имя и Фамилия больше или равно 3
    if (nameLength >= 2) {
        console.log("ok");
        // Если символов нет вообще
    } else if (NameSymbols === "") {
        // То ставим классы для показа ошибки и печатаем ошибку
        mistakeName.classList.add("input-mistake-name-color");
        inputName.classList.add("input1-mistake");
        mistakeName.innerHTML = "Вы забыли указать имя и Фамилию";
        return;
    } else {
        // В противном случае, если символов меньше 3-х, то также ставим классы для ошибки и печатаем ошибку
        mistakeName.classList.add("input-mistake-name-color");
        inputName.classList.add("input1-mistake");
        mistakeName.innerHTML = "Имя не может быть короче двух символов";
        return;
    }


    // Проверяем оценка отзыва от 0 до 5
    if (grade > 0 && grade < 6) {
        console.log("OK");
    } else {
        // Если ни одно условие не верно, то ставим классы для ошибки и печатаем ошибку 
        mistakeGrade.classList.add("input-mistake-grade-color");
        inputGrade.classList.add("input2-mistake");
        mistakeGrade.innerHTML = "Оценка должна быть от 1 до 5";
        return;
    }

    // Обновляем форму
    form.reset();
    // Удаляем из локальной сессии браузера ключи name, grade и reviewText
    localStorage.removeItem("name");
    localStorage.removeItem("grade");
    console.log("ok");
}

// Описываем функцию чистки ошибок
function clearmistakeName() {
    mistakeName.classList.remove("input-mistake-name-color");
    inputName.classList.remove("input1-mistake");
    mistakeGrade.classList.remove("input-mistake-grade-color");
    inputGrade.classList.remove("input2-mistake");

    mistakeName.innerHTML = "";
    mistakeGrade.innerHTML = "";
}


function clearmistakeGrade() {
    mistakeGrade.classList.remove("input-mistake-grade-color");
    inputGrade.classList.remove("input2-mistake");
    mistakeGrade.innerHTML = "";
    mistakeReview.classList.remove("input-mistake-review-color");
    inputReview.classList.remove("textarea-mistake");
    mistakeReview.innerHTML = "";
}

function changeName() {
    localStorage.setItem("name", inputName.value);
}

function changeGrade() {
    localStorage.setItem("grade", inputGrade.value);
}

// Создаём переменные под хранения элементов из html
let sidebar = document.querySelector(".adblock");
let like = document.querySelector(".adblock__like");
let choiseButton = document.querySelector(".adblock > .adblock__cart");

let header = document.querySelector(".header-section");
let likeNumber = document.querySelector(".cart-number_hidden");
let CartNumber = document.querySelector(".heart-number_hidden");

let message = document.querySelector(".message-cart_hidden");

// Считаем количество лайков (добавление в избранное)
like.value = +localStorage.getItem("likeCount");
// Считаем количество товаров, добавленных в корзину
choiseButton.value = +localStorage.getItem("productCount");

// При клике мышкой на "сердечко" вызываем метод changeLike
like.addEventListener("click", changeLike);

// При клике мышкой на "добавить в корзину" вызываем метод changeCart 
choiseButton.addEventListener("click", changeCart);

console.log(like.value);
console.log(+choiseButton.value);

// Описание метода changeLike
function changeLike(event) {
    // Если количество лайков равно 0, то при нажатии надо поставить лайк
    if (like.value === 0) {
        like.value += 1;
        console.log(like.value);

        like.classList.add("adblock__like_focus");
        likeNumber.classList.add("cart-number");

        localStorage.setItem("likeCount", like.value);
    } else {
        // Если количество лайков не равно 0, значит лайк уже стоит, при нажатии его надо убрать
        // Отнимаем от количества лайков единицу
        like.value -= 1;
        console.log(like.value);

        like.classList.remove("adblock__like_focus");
        likeNumber.classList.remove("cart-number");

        localStorage.removeItem("likeCount", like.value);
        return;
    }
}

// Описание метода changeCart
function changeCart(event) {
    // Если количество товаров равно 0, то при нажатии надо добавить товар
    if (+choiseButton.value === 0) {
        // Прибавляем к количеству товаров 1
        choiseButton.value += 1;
        console.log(+choiseButton.value);
        message.classList.add("message-cart");
        // Ставим таймер, который через 3 секунды удаляет этот класс для добавленного товара
        setTimeout(function () {
            message.classList.remove("message-cart");
        }, 3000);

        // Добавляем другие классы для кнопки, что товар добавлен
        choiseButton.classList.add("adblock__cart_choise");
        CartNumber.classList.add("heart-number");
        // Пишем в кнопку текст - Товар уже в корзине
        choiseButton.innerHTML = "Товар уже в корзине";
        // Обновляем в сессию количество товаров в ключ productCount
        localStorage.setItem("productCount", +choiseButton.value);
    } else {
        // Если количество товаров не равно нулю, значит товар уже есть в корзине и его надо убрать
        // Отнимаем у количества товаров единицу
        choiseButton.value -= 1;
        console.log(+choiseButton.value);
        // Удаляем классы с добавленным товаром
        choiseButton.classList.remove("adblock__cart_choise");
        CartNumber.classList.remove("heart-number");
        message.classList.remove("message-cart");
        // Возращаем текст кнопки - "Добавить в корзину"
        choiseButton.innerHTML = "Добавить в корзину";
        // Обновляем в сессию количество товаров в ключ productCount
        localStorage.removeItem("productCount", +choiseButton.value);
        return;
    }
}

// Запускаем прослушку страницы
document.addEventListener("DOMContentLoaded", () => {
    // При запуске страницы магазина проверяем количество лайков
    if (like.value === 0) {
        // Если количество лайков равно 0 удаляем классы с добавленным лайком
        like.classList.remove("adblock__like_focus");
        likeNumber.classList.remove("cart-number");
    } else {
        // И наоборот, если количество лайков не равно 0 добавляем классы с добавленным лайком
        like.classList.add("adblock__like_focus");
        likeNumber.classList.add("cart-number");
    }

    // Аналогично проверяем количество товаров
    if (+choiseButton.value === 0) {
        choiseButton.classList.remove("adblock__cart_choise");
        CartNumber.classList.remove("heart-number");
        choiseButton.innerHTML = "Добавить в корзину";
    } else {
        choiseButton.classList.add("adblock__cart_choise");
        CartNumber.classList.add("heart-number");
        choiseButton.innerHTML = "Товар уже в корзине";
        return;
    }
});
