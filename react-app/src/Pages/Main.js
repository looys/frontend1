import React from "react";
import './Main.css';
import { Link } from "react-router-dom"

function Main() {
    return (
        <div className="main-page">
            <div className="text">
            Здесь должно быть содержимое главной страницы.<br></br>
            Но в рамках курса главная страница  используется лишь <br></br>
            в демонстрационных целях
            </div>
            <Link to="/product" className="link-page">
                Перейти на страницу товара
            </Link>
        </div>

    )
}

export default Main;