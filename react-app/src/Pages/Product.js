import React from "react";
import Centered from "../components/Centered/Centered";
import MainImage from "../components/MainImage/MainImage";
import Navigation from "../components/Navigation/Navigation";

function Product() {
    return (
        <>
        <Navigation />
        <MainImage />
        <Centered />
        </>
    )
}

export default Product;