const buttonsValues = [
  '128 Гб',
  '256 Гб',
  '512 Гб',
]

const buttonsObjects = [
  {
    id: 1,
    name: '128 Гб',
  },
  {
    id: 2,
    name: '256 Гб',
  },
  {
    id: 3,
    name: '512 Гб',
  },
]

const colorButtons = [
  {
    src: 'static/color-1.webp',
    className: 'product-color__img',
  },
  {
    src: 'static/color-2.webp',
    className: 'product-color__img',
  },
  {
    src: 'static/color-3.webp',
    className: 'product-color__img',
  },
  {
    src: 'static/color-4.webp',
    className: 'product-color__img_selected',
  },
  {
    src: 'static/color-5.webp',
    className: 'product-color__img',
  },
  {
    src: 'static/color-6.webp',
    className: 'product-color__img',
  },
]

const reviewsData = [
  {
    author: 'Марк Г.',
    score: 5,
    avatar: 'static/img-reviews/Authorphoto-1.png',
    usageExperience: 'менее месяца',
    advantage: 'это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.',
    disadvantages: 'к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное',
  },
  {
    author: 'Валерий Коваленко',
    avatar: 'static/img-reviews/Authorphoto-2.png',
    score: 4,
    usageExperience: 'менее месяца',
    advantage: 'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
    disadvantages: 'Плохая ремонтопригодность',
  },
]

export { buttonsValues, buttonsObjects, reviewsData, colorButtons }

