import React from 'react';

import './App.css';

import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Main from './Pages/Main';
import Product from './Pages/Product';




function App() {

  return (
    <div className="App">

      <Header />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/product" element={<Product />} />
        </Routes>
      </BrowserRouter>
      <Footer />


    </div>
  );
}

export default App;
