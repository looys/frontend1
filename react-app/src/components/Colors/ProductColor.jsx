import React from 'react'
import ColorButton from './ColorButton'
import './ProductColor.css'

function ProductColor(props) {

    let { button } = props


    return (
        <>
            <section className="product-color">
                <h4 className="product-color__name title-h4">
                    Цвет товара: синий
                </h4>
                <div className="product-color__images">

                    {button.map(({ src, className }) =>

                        <ColorButton src={src} className={className} />
                    )
                    }
                </div>


            </section>
        </>

    )
}

export default ProductColor;

    // <>
    // < section className="color-phone" >
    //     <h4 className="color-phone__title title-h4">
    //         Цвет товара: синий
    //     </h4>
    //     <div className="color-phone__avatars">
    //         <button className="row-avatars">
    //             <img className="small" src='static/color-1.webp' alt="телефон красный" />
    //         </button>
    //         <button className="row-avatars">
    //             <img className="small" src='static/color-2.webp' alt="телефон зеленый" />
    //         </button>
    //         <button className="row-avatars">
    //             <img className="small" src='static/color-3.webp' alt="телефон розовый" />
    //         </button>
    //         <button className="row-avatars selected">
    //             <img className="small" src='static/color-4.webp' alt="телефон синий" />
    //         </button>
    //         <button className="row-avatars">
    //             <img className="small" src='static/color-5.webp' alt="телефон белый" />
    //         </button>
    //         <button className="row-avatars">
    //             <img className="small" src='static/color-6.webp' alt="телефон черный" />
    //         </button>
    //     </div>
    // </section >