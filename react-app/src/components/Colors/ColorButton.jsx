import './ProductColor.css'

function ColorButton(props) {
    let { className, src} = props;
    
    return (
        <div className={className}>
            <img className="img-button" src={src} alt="Цвет телефона" key={src}/>
        </div>
    );
}

export default ColorButton;