import Iframe from '../IFrame/IFrame';
import './Sidebar.css';
import { useSelect, useDispatch } from "react-redux"
import { addProduct } from '../../store/cart-reducer'

function Sidebar(props) {
    const { handleClickButton, handleClickLike, ActiveButton, ActiveLike } =
        props;
    // const dispatc = useDispatch();

    // function handleClickAdd() {
    //     const btn = document.querySelector('.adblock__cart');

    //     btn.classList.togglet('added');

    //     if (btn.classList.contains('added')) {
    //         const action = addProduct({id: 4884});

    //         dispatc(action);
    //     }
    // }

    return (

        <div className="right">
            <div className="adblock">

                <div className="adblock__price">
                    <div className="block-price">
                        <div className="adblock__oldprice">75 990₽</div>
                        <button className="adblock__pricebtn">-8%</button>
                    </div>
                    <div
                        onClick={handleClickLike}
                        className={`sidebar-info__like ${ActiveLike ? "like_choise" : ""
                            }`}
                    ></div>
                    {/* <svg className="adblock__like" width="28" height="22" viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M2.78502 2.57269C5.17872 0.27474 9.04661 0.27474 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.27474 22.8216 0.27474 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63044 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63044 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" fill="#888888" />
                    </svg> */}

                </div>

                <div className="adblock__newprice">67 990₽</div>
                <div className="adblock__shipping">
                    Самовывоз в четверг, 1 сентября — <span className="bold">бесплатно</span>
                </div>
                <div className="adblock__shipping1">
                    Курьером в четверг, 1 сентября — <span className="bold">бесплатно</span>
                </div>
                <button 
                onClick={handleClickButton}
                className={`adblock__cart ${ActiveButton ? "choise" : ""}`}
                >
                    Добавить в корзину</button>
            </div>

            <h4 class="adblock__rec-color">Реклама</h4>
            <div class="adblock__ifr">
                <div class="ifr__content">
                    <Iframe />
                </div>
                <div class="ifr__content">
                    <Iframe />
                </div>
            </div>


            {/* <div className="message-cart_hidden"> Товар добавлен в корзину</div> */}
        </div>
    );
}

export default Sidebar;

// кнопка добавить в корзину className="adblock__cart"