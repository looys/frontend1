import React from 'react';
// import { Link } from 'react-router-dom';
import logo from '../../logo.svg';

import './Header.css';

function Header(props) {
    const { ActiveButton, ActiveLike } = props;
    return (
        // <div className='container'>
            <header className='header-section'>
                {/* <Link to="/"> */}
                <div className="header-section__name">
                    <div className="header-section__logo">
                        <img src={logo} alt="логотип" />
                    </div>
                    <h1 className="header-section__title">
                        <span className="color">Мой</span>Маркет
                    </h1>
                </div>
                {/* </Link> */}
                <div className="header-section__icons">
                    <div className={`heart-number ${ActiveButton ? "" : "hidden"}`}>
                        1
                    </div>
                    <div className={`cart-number ${ActiveLike ? "" : "hidden"}`}>
                        1
                    </div>
                    <img className="heart-icon" src='static/heart.png' alt="значок сердечко" />
                    <img className="cart-icon" src='static/cart.png' alt="значок корзинка" />
                </div>
            </header>
        // </div>

    );
}

export default Header;

