import { useState } from "react";
import ReviewItem from "./ReviewItem";
import { reviewsData } from "../../data/data";
import './Reviews.css';

function ReviewList() {
    const [state, setState] = useState(reviewsData)
    return (
        <section className="review-section">
            <div className="review-section__header">
                <div className="review-section__text">
                    <h3 className="review-section__title">Отзывы</h3>
                    <span className="review-section__count">425</span>
                </div>
            </div>
            <div className="review-section__list">
                {state && state.map(review => (
                    <ReviewItem
                        review={review}
                    />
                ))}

            </div>

        </section>
    );
}

export default ReviewList;