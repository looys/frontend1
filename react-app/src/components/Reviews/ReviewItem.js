
import ReviewStars from "../ReviewStars/ReviewStars";

function ReviewItem({ review }) {
    const { author, score, avatar, usageExperience, advantage, disadvantages } = review;
    return (
        <>
            <div className="review">
                <img src={avatar} className="review__photo"
                    alt="фото автора отзыва" />
                <div className="review__content">
                    <h4 className="review__name">{author}</h4>
                    <ReviewStars score={score} />
                    <div className="review__parameters">
                        <div className="review__parameter"><strong>Опыт использования:</strong>
                            {usageExperience}
                        </div>
                        <div className="review__parameter">
                            <div>
                                <strong>Достоинства:</strong>
                            </div>
                            {advantage}
                        </div>
                        <div className="review__parameter">
                            <div>
                                <strong>Недостатки:</strong>
                            </div>
                            {disadvantages}
                        </div>
                    </div>
                </div>
            </div>
            <div className="separator"></div>
        </>
    );
}

export default ReviewItem;