import './MainImage.css'

function MainImage() {
    return (
        <div className="main">
            <section className="main__name">
                <h2 className="main__name_title">
                    Смартфон Apple iPhone 13, синий
                </h2>
            </section>

            <section className="main__img-phone">
                <div className="row">
                    <div className="img-row">
                        <img className="img-row_big" src='static/image-1.webp' alt="вид телефона-1" />
                    </div>
                    <div className="img-row">
                        <img className="img-row_big" src='static/image-2.webp' alt="вид телефона-2" />
                    </div>
                    <div className="img-row">
                        <img className="img-row_big" src='static/image-3.webp' alt="вид телефона-3" />
                    </div>
                    <div className="img-row">
                        <img className="img-row_big" src='static/image-4.webp' alt="вид телефона-4" />
                    </div>
                    <div className="img-row">
                        <img className="img-row_big" src='static/image-5.webp' alt="вид телефона-5" />
                    </div>
                </div>
            </section>
        </div>

    )
}

export default MainImage



