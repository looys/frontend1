import React from 'react';

import './PageProduct.css';

import ReviewList from '../Reviews/ReviewList';
import Form from '../Form/Form';
import Config from '../Config/Config';
import ProductColor from '../Colors/ProductColor';
import { colorButtons } from '../../data/data';


function PageProduct() {
    return (
        <>
            <section className="main__content">
                <div className="row">
                    <div className="left">

                        <ProductColor button={colorButtons} />

                        <Config />

                        <section className="characteristics">
                            <h4 className="characteristics__title title-h4">
                                Характеристики товара
                            </h4>
                            <ul className="characteristics__list">
                                <li className="characteristics__item">
                                    Экран: <span className="bold">6.1</span>
                                </li>
                                <li className="characteristics__item">
                                    Встроенная память: <span className="bold">128 ГБ</span>
                                </li>
                                <li className="characteristics__item">
                                    Операционная система: <span className="bold">iOS 15</span>
                                </li>
                                <li className="characteristics__item">
                                    Беспроводные интерфейсы: <span className="bold">NFC, Bluetooth, Wi-Fi</span>
                                </li>
                                <li className="characteristics__item">
                                    Процессор: <a className="bold link" href="https://ru.wikipedia.org/wiki/Apple_A15"
                                        target="_blank">Apple A15
                                        Bionic</a>
                                </li>
                                <li className="characteristics__item">
                                    Вес: <span className="bold">173 г</span>
                                </li>
                            </ul>
                        </section>

                        <section className="description">
                            <h4 className="description__title title-h4">
                                Описание
                            </h4>
                            <div className="description__paragraphs">
                                <p className="paragraph">
                                    Наша самая совершенная система двух камер.<br></br>
                                    Особый взгляд на прочность дисплея.<br></br>
                                    Чип, с которым всё супербыстро.<br></br>
                                    Аккумулятор держится заметно дольше.<br></br>
                                    <span className="i">iPhone 13 - сильный мира всего.</span><br></br>
                                </p>
                                <p className="paragraph">
                                    Мы разработали совершенно новую схему расположения и развернули объективы на 45
                                    градусов.
                                    Благодаря
                                    этому внутри корпуса поместилась наша лучшая система двух камер с увеличенной
                                    матрицей
                                    широкоугольной
                                    камеры. Кроме того, мы освободили место для системы оптической стабилизации
                                    изображения
                                    сдвигом
                                    матрицы.
                                    И повысили скорость работы матрицы на сверхширокоугольной камере.
                                </p>
                                <p className="paragraph">
                                    Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков.
                                    Новая
                                    широкоугольная
                                    камера улавливает на 47% больше света для более качественных фотографий и видео.
                                    Новая
                                    оптическая
                                    стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом
                                    положении.
                                </p>
                                <p className="paragraph">
                                    Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и
                                    изменения
                                    резкости.
                                    Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте
                                    съёмки,
                                    создавая
                                    красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно
                                    перевести
                                    фокус
                                    на
                                    другого человека или объект, который появился в кадре. Теперь ваши видео будут
                                    смотреться
                                    как
                                    настоящее
                                    кино.
                                </p>
                            </div>
                        </section>

                        <section className="comparison-table">
                            <h4 className="comparison-table__title title-h4">Сравнение моделей</h4>
                            <table className="comparison-table__models">
                                <thead>
                                    <tr>
                                        <th>Модель</th>
                                        <th>Вес</th>
                                        <th>Высота</th>
                                        <th>Ширина</th>
                                        <th>Толщина</th>
                                        <th>Чип</th>
                                        <th>Объём памяти</th>
                                        <th>Аккумулятор</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>iPhone 11</td>
                                        <td>194 грамма</td>
                                        <td>150.9 мм</td>
                                        <td>75.7 мм</td>
                                        <td>8.3 мм</td>
                                        <td>A13 Bionic chip</td>
                                        <td>до 128 Гб</td>
                                        <td>До 17 часов</td>
                                    </tr>
                                    <tr>
                                        <td>iPhone 12</td>
                                        <td>164 грамма</td>
                                        <td>146.7 мм</td>
                                        <td>71.5 мм</td>
                                        <td>7.4 мм</td>
                                        <td>A14 Bionic chip</td>
                                        <td>до 256 Гб</td>
                                        <td>До 19 часов</td>
                                    </tr>
                                    <tr>
                                        <td>iPhone 13</td>
                                        <td>174 грамма</td>
                                        <td>146.7 мм</td>
                                        <td>71.5 мм</td>
                                        <td>7.65 мм</td>
                                        <td>A15 Bionic chip</td>
                                        <td>до 512 Гб</td>
                                        <td>До 19 часов</td>
                                    </tr>
                                </tbody>
                            </table>
                        </section>

                        <ReviewList />

                        <Form />

                    </div>

                </div>

            </section>
        </>

    );
}

export default PageProduct;