import './Navigation.css';

function Navigation() {
    return (
        <nav className="navigation container">
            <div className="navigation__link">
                <a className="link" href="">Электроника</a>
                <span className="navigation__symbol"> > </span>
                <a className="link" href="">Смартфоны и гаджеты</a>
                <span className="navigation__symbol"> > </span>
                <a className="link" href="">Мобильные телефоны</a>
                <span className="navigation__symbol"> > </span>
                <a className="link" href="">Apple</a>
            </div>
        </nav>
    )
}

export default Navigation;