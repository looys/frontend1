import React from 'react'

function ReviewStars({score}) {
    
    return (
        <div className="star-rating">
      {[...Array(5)].map((el, index) => {
        if (index < score) {
          return (
            <img src={'static/Img-reviews/Star-gold.png'} alt="звезда" />
          );
        }

        return (
          <img src={'static/Img-reviews/Star-grey.png'} alt="звезда"/>
        )
      })}
    </div>

    )
}
export default ReviewStars