import { useState } from 'react';
import './Form.css';

let errorInitial = { name: null, rating: null };

function Form() {
    let [input, setInput] = useState(JSON.parse(localStorage.getItem('reviewFormInput')));
    let [error, setError] = useState({ errorInitial });
    localStorage.setItem('reviewFormInput', JSON.stringify(input));

    // console.log(input);

    let handleSubmit = (event) => {
        event.preventDefault();

        if (input?.name.trim() === '') {
            setError({ ...error, name: 'Вы забыли указать имя и фамилию' });
            return;
        }

        let nameLength = input?.name.trim().length;
        if (nameLength < 2) {
            setError({ ...error, name: 'Имя не может быть короче 2-х символов' });
            return;
        }

        let rating = +input?.rating;
        if (isNaN(rating) || rating < 1 || rating > 5) {
            setError({ ...error, rating: 'Оценка должна быть от 1 до 5' });
            return;
        }

        setInput({ name: "", rating: "", text: "" });
        console.log("Проверка завершена");
    };

    // Срабатывает при вводе имени
    let handleInputName = (event) => {
        setInput({ ...input, name: event.target.value });
    };

    // Срабатывает при фокусе на поле с именем
    let handleFocusName = () => {
        setError({ ...errorInitial })
    };


    // Срабатывает при вводе рейтинга 
    let handleInputRating = (event) => {
        setInput({ ...input, rating: event.target.value });
    };

    // Срабатывает при фокусе на поле с рейтингом
    let handleFocusRating = () => {
        setError({ ...errorInitial })
    }

    // Срабатывает при вводе текста отзыва
    let handleInputText = (event) => {
        setInput({ ...input, text: event.target.value });
    };
    // Срабатывает при фокусе на поле с текстом отзыва
    // let handleFocusText = () => { }



    return (
        <form className="form margin-bottom-100" onSubmit={handleSubmit}>
            <h4 className="form__subtitle title-h4">
                Добавить свой отзыв
            </h4>
            <div className="form__input">
                <div className="input__fieldset1">
                    <input
                        className="form__name form-hover-focus"
                        type='text'
                        name='name'
                        placeholder="Имя и фамилия"
                        value={input?.name}
                        onInput={handleInputName}
                        onFocus={handleFocusName}
                    />
                    <div className="error-blok-name">
                        <div className={`error-name ${error.name ? '' : 'hidden'}`}>{error.name}</div>
                    </div>
                </div>
                <div className="input__fieldset2"></div>
                <div className="input__fieldset3">
                    <input
                        className="form__grade form-hover-focus"
                        type="text"
                        name="grade"
                        placeholder="Оценка"
                        value={input?.rating}
                        onInput={handleInputRating}
                        onFocus={handleFocusRating}
                    />
                    <div className="error-blok-rating3">
                        <div className={`error-rating ${error.rating ? '' : 'hidden'}`}>{error.rating}</div>
                    </div>
                </div>
            </div>
            
                <textarea
                    className="review__text-author form-hover-focus"
                    name='text'
                    placeholder="Текст отзыва"
                    value={input?.text}
                    onInput={handleInputText}>
                </textarea>
            

            <button className="reviews-form__submit1" type="submit">Отправить отзыв</button>

        </form >
    );
}

export default Form;


