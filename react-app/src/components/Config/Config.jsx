import React, { useEffect, useState } from 'react'
import { buttonsValues, buttonsObjects } from '../../data/data'
import './Config.css'

function Config() {

    const [values, setValues] = useState(buttonsValues);
    const [selectedIdx, setSelectedIdx] = useState(0);

    const handleClick = (index) => {
        setSelectedIdx(index)
    }

    return (
        <>
            <section className="config">
                <h4 className="config__title title-h4">
                    Конфигурация памяти: 128 ГБ
                </h4>
                <div className="config">
                    <div className='config__buttons'>
                        {buttonsObjects.map(({ id, name }, idx) => (
                            <button
                                className={
                                    idx === selectedIdx ? 'btn btn_selected' : 'btn'
                                }
                                onClick={() => handleClick(idx)}
                                key={id}
                            >
                                {name}
                            </button>
                        ))}
                    </div>

                </div>
            </section>
        </>

    )
}

export default Config