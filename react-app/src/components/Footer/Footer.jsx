import React from 'react';

import './Footer.css';

function Footer() {
    return (
        <footer className="footer">
        <div className="container">
            <div className="footer__text">
                <div className="footer__copyright">
                    <div className="bold">
                        © ООО «<span className="color">Мой</span>Маркет», 2018-2022.
                    </div>
                    <div>
                        Для уточнения информации звоните по номеру <a className="link" href="tel:79000000000">+7 900 000
                            0000</a>,
                    </div>
                    <div>
                        а предложения по сотрудничеству отправляйте на почту <a className="link"
                            href="mailto:partner@mymarket.com">partner@mymarket.com</a>
                    </div>
                </div>

                <div className="footer__up">
                    <a className="link" href="#top">Наверх</a>
                </div>
            </div>
        </div>
    </footer>


    );
}

export default Footer;