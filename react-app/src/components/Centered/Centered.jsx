import Sidebar from "../Sidebar/Sidebar";
import PageProduct from "../PageProduct/PageProduct";
import "./Centered.css";

function Centered() {
    return (
        <div className="container">
            <div className="wrapper row">
                <div className="left">
                    <PageProduct />
                </div>
                <div className="right">
                    <Sidebar />
                </div>
            </div>


        </div>

    )
}

export default Centered