import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
    name: "cart",

    // Начальное состояние хранилища, товаров нет
    initialState: {
        products: [],
    },
    // Все доступные метода
    reducers: {
        // Добавить товар, первый параметр это текущее состояние
        // А второй параметр имеет данные для действия
        addProduct: (prevState, action) => {
            return {
                ...prevState,
                // Внутри action.payload информация о добавленном товаре
                // Возвращаем новый массив товаров вместе с добавленным
                products: [...prevState.products, action.payload],
            };
        },
    },
});
// Экспортируем наружу все действия
export const { addProduct } = cartSlice.actions;
// И сам редуктор тоже
export default cartSlice.reducer;