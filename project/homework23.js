"use strict";

// Упражнение 1

let count = +prompt('Введите число');
if (!count) alert('Ошибка, вы ввели не число');

let intervalId = setInterval(() => {
    count = count - 1;

    console.log(`Осталось ${count}`);

    if (count === 0) {


        clearInterval(intervalId);
        console.log(`Время вышло!`)
    }
}, 1000);


// Упражнение 2

let promise = fetch("https://reqres.in/api/users");

promise
    .then(function (response) {
        return response.json();
    })

    .then(function (response) {
        let users = response.data;

        console.log(`Получили пользователей: ${users.length}`)
        users.forEach(function (user) {
            console.log(`- ${user.first_name} ${user.last_name} (${user.email})`);
        });
    })
