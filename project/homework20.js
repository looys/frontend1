"use strict";

// Упражнение 1
for (let a = 0; a <= 20; a++) {
    if (a % 2 == 0) {
        console.log(a);
    }
}

// Упражнение 2
let sum = 0;
let count = 0;

while (count !== 3) {
    let value = +prompt("Введите число", "");
    if (!value) alert('Ошибка, вы ввели не число');
    if (!value) break;
    sum += value;
    count++;
}
if (count == 3) {
    alert("Сумма ваших чисел: " + sum);
}


// Упражнение 3
function getNameOfMonth(m) {
    if (m === 0) return 'Январь';
    if (m === 1) return 'Февраль';
    if (m === 2) return 'Март';
    if (m === 3) return 'Апрель';
    if (m === 4) return 'Май';
    if (m === 5) return 'Июнь';
    if (m === 6) return 'Июль';
    if (m === 7) return 'Август';
    if (m === 8) return 'Сентябрь';
    if (m === 9) return 'Октябрь';
    if (m === 10) return 'Ноябрь';
    if (m === 11) return 'Декабрь';
}

for (let m = 0; m < 12; m++) {
    const month = getNameOfMonth(m);

    if (month === 'Октябрь') continue;

    console.log(month);
}

