"use strict";

// Упражнение 1

let obj = {
    //name: 'John',
};

/**
 * @param {object} nameObj
 * @param {string} key
 * @returns true, если у объекта нет свойств
 */
function isEmpty(nameObj) {
    for (let key in nameObj) {
        return false;
    }
    return true;
}

console.log(isEmpty(obj));


// Упражнение 2
// файл data.js


// Упражнение 3

let salaries = {
    John:100000,
    Ann:160000,
    Pete:130000,
};

function raiseSalary(perzent) {
    let newSalaries = {};
    for (let key in salaries) {
        let raise = (salaries[key] * perzent) / 100;

        newSalaries[key] = salaries[key] + raise;
    }
    return newSalaries;
}

function calcSumm(obj) {
    let summ = 0;
    for (let key in obj) {
        summ = summ + obj[key];
    }
    return summ;
}

let result = raiseSalary(5);
let summ = calcSumm(result)

console.log(salaries, result, summ);